package main

import (
    "fmt"
    "log"
    "net/http"
    "os"
)

const (
    ENV_ORIGIN_PORT = "ORIGIN_PORT"
)

const (
    SERV_PORT = "8090"
)


func main() {
    // Get service port
    servPort := os.Getenv(ENV_ORIGIN_PORT)
    if servPort == "" {
            servPort = SERV_PORT
    }
    http.HandleFunc("/books", func(w http.ResponseWriter, r *http.Request) {
        if r.Method == "GET" {
            w.WriteHeader(http.StatusOK)
            str := "\n--> GET\n\n"
            fmt.Fprintf(w, str)
        } else if r.Method == "POST" {
            w.WriteHeader(http.StatusOK)
            str := "\n--> POST\n\n"
            fmt.Fprintf(w, str)
        } else {
            w.WriteHeader(http.StatusBadRequest)
        }
    })

    // Start server
    log.Fatal(http.ListenAndServe(":" + SERV_PORT, nil))
}
