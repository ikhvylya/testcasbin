package main

import (
    "errors"
    "fmt"
    "github.com/casbin/casbin"
    "github.com/casbin/casbin/persist/file-adapter"
    "log"
    "net/http"
    "net/http/httputil"
    "net/url"
    "os"
)

//
// Const
//
const (
    ENV_ORIGIN_HOST = "ORIGIN_HOST"
    ENV_ORIGIN_PORT = "ORIGIN_PORT"
    ENV_ORIGIN_APP  = "ORIGIN_APP"

    ENV_PROXY_PORT = "PROXY_PORT"
)

const (
    ORIGIN_HOST = "localhost"
    ORIGIN_PORT = "8090"
    ORIGIN_APP  = "authhttp"

    SERV_PORT = "8080"
)

func main() {

    // User Name to User Role map
    usersInfo := map[string]string{
        "admin" : "admin",
        "user1": "ro",
        "user2": "rw",
        "radmin": "radmin",
        "ruser1": "rro",
        "ruser2": "rrw",
    }

    // Get remote service ENV
    originHost := os.Getenv(ENV_ORIGIN_HOST)
    originPort := os.Getenv(ENV_ORIGIN_PORT)
    originApp  := os.Getenv(ENV_ORIGIN_APP)

    servPort := os.Getenv(ENV_PROXY_PORT)

    //
    // Set Vars
    //
    {
        // Origin
        if originHost == "" {
            originHost = ORIGIN_HOST
        }
        if originPort == "" {
            originPort = ORIGIN_PORT
        }
        if originApp == "" {
            originApp = ORIGIN_APP
        }

        // Local
        if servPort == "" {
            servPort = SERV_PORT
        }
    }
    //
    // Init CASBIN
    //
    authEnforcer := casbin.NewEnforcer()
    adapter := fileadapter.NewFilteredAdapter("./policy.csv")
    authEnforcer.InitWithAdapter("./auth_model.conf", adapter)

    filter := &fileadapter.Filter{
        P: []string{"", originApp},
    }
    authEnforcer.LoadFilteredPolicy(filter)
    subjects := authEnforcer.GetAllSubjects()
    // DEBUG
    log.Printf("FILTERING ...")
    for _, subj := range subjects {
        fmt.Println("\tROLE:", subj)
    }
    //
    // Start the server
    //
    originURL := fmt.Sprintf("http://%v:%v/", originHost, originPort)
    u, err := url.Parse(originURL)
    if err != nil {
        log.Fatal("Error parsing URL")
    }
    reverseProxy := httputil.NewSingleHostReverseProxy(u)


    http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {

        // Get user name
        userName := r.URL.Query().Get("user")
        if userName == "" {
            writeError(http.StatusForbidden, "FORBIDDEN", w, errors.New("unauthorized"))
            return
        }
        // DEBUG
        log.Printf("USER: %v", userName)
        // Get user role
        userRole, ok := usersInfo[userName]
        if !ok {
            writeError(http.StatusForbidden, "FORBIDDEN", w, errors.New("unauthorized"))
            return
        }
        // DEBUG
        log.Printf("ROLE: %v", userRole)
        log.Printf("PATH: %v", r.URL.Path)
        log.Printf("METHOD: %v", r.Method)
        // Casbin rule enforcing
        res, err := authEnforcer.EnforceSafe(userRole, originApp, r.URL.Path, r.Method)
        if err != nil {
            writeError(http.StatusInternalServerError, "ERROR", w, err)
            return
        }
        if res {
            reverseProxy.ServeHTTP(w, r)
        } else {
            writeError(http.StatusForbidden, "FORBIDDEN", w, errors.New("unauthorized"))
        }
    })

    // Start the server
    addr := ":" + servPort
    log.Fatal(http.ListenAndServe(addr, nil))
}

//
// Helpers
//
func writeError(status int, message string, w http.ResponseWriter, err error) {
    log.Print("ERROR: ", err.Error())
    w.WriteHeader(status)
    w.Write([]byte(message + "\n"))
}

func writeSuccess(message string, w http.ResponseWriter) {
    w.WriteHeader(http.StatusOK)
    w.Write([]byte(message + "\n"))
}
